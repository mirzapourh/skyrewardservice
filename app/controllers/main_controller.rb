=begin

    Author: Hossein Mirzapour
  
    Main Controller dealing with user interface parameters and obtaining results from services


=end

class MainController < ApplicationController

  # include MainUtilities
  # Parameters sent from the view

  def form_input

    account_number = params[:account_number]

    portfolio = params[:portfolio]


    customer = SkyEligibilityService.new account_number
    

    begin

        if customer.eligible?

            reward_service = SkyRewardService.new account_number, portfolio
            
            redirect_to  main_reward_service_output_path status: "success", message: "You Are Eligible!" , result: reward_service.rewards

        else

            redirect_to  main_reward_service_output_path status: "danger", message: "You Are Not Eligible!" , result: "No Reward"

        end


    rescue Exception => e
         
         redirect_to  main_reward_service_output_path status: "danger", message: "Error" , result: e

    end 


    end

    # RewardServiceInterface.new account_number, portfolio


  def reward_service_output 

    @result = params[:result]
    @status = params[:status]
    @message = params[:message]

  end

end
