class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :portfolio_map


  def portfolio_map

    @portfolios = Array.new

    Portfolio.all.each do |portfolio|

      @portfolios << [ portfolio.name, portfolio.id]

    end

  end

end
