class Customer < ActiveRecord::Base

  GOOD_BILLING_STATUS = "GOOD" # Either GOOD or BAD
  ACCEPTABLE_LOYALTY_RATING = 2 # 1..5

  attr_reader :loyalty_rating, :billing_status

  has_many :customer_subscriptions
  has_many :channel_subscriptions, through: :customer_subscriptions


  def self.loyal?

    loyalty_rating >= ACCEPTABLE_LOYALTY_RATING

  end

  def self.has_good_billing_status?

    billing_status == GOOD_BILLING_STATUS

  end
end
