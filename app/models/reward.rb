=begin

  Author: Hossein Mirzapour 

  Reward class provides an Enumerable for rewards associated to subscriptions

=end

class Reward

  CHAMPIONS_LEAGUE_FINAL_TICKET = "Champions League Final Ticket"
  KARAOKE_PRO_MICROPHONE = "Karaoke Pro Microphone"
  PIRATES_OF_THE_CARIBBEAN_COLLECTION = "Pirates Of The Caribbean Collection"
  REWARD_NOT_AVAILABLE = "No Rewards"

  include Enumerable

  def each
    yield   ["SPORTS",  CHAMPIONS_LEAGUE_FINAL_TICKET]
    yield   ["KIDS",  REWARD_NOT_AVAILABLE]
    yield   ["MUSIC",  KARAOKE_PRO_MICROPHONE]
    yield   ["NEWS",  REWARD_NOT_AVAILABLE]
    yield   ["MOVIES",  PIRATES_OF_THE_CARIBBEAN_COLLECTION]
  end

end
