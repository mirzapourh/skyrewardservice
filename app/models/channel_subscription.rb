class ChannelSubscription < ActiveRecord::Base

  has_many :customer_subscriptions
  has_many :customers, through: :customer_subscriptions

  belongs_to :portfolio 

end
