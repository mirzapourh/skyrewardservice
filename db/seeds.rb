# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# CHAMPIONS_LEAGUE_FINAL_TICKET = "Champions League Final Ticket"
# KARAOKE_PRO_MICROPHONE = "Karaoke Pro Microphone"
# PIRATES_OF_THE_CARIBBEAN_COLLECTION = "Pirates Of The Caribbean Collection"
# REWARD_NOT_AVAILABLE = "No Rewards"


# ["SPORTS",  CHAMPIONS_LEAGUE_FINAL_TICKET]
# ["KIDS",  REWARD_NOT_AVAILABLE]
# ["MUSIC",  KARAOKE_PRO_MICROPHONE]
# ["NEWS",  REWARD_NOT_AVAILABLE]
# ["MOVIES",  PIRATES_OF_THE_CARIBBEAN_COLLECTION]



############################################################ Subscription

loop_counter = 1 
channels = ['SPORTS', 'KIDS', 'MUSIC', 'NEWS', 'MOVIES']

all_possibile_portfolio_combinations = Array.new

while loop_counter <= channels.size

  channels.combination(loop_counter).to_a.each do |inner_arrays|

    all_possibile_portfolio_combinations << inner_arrays

  end

  loop_counter += 1

end

all_possibile_portfolio_combinations.each do |subscriptions|

  portfolio = Portfolio.new

  portfolio.name = "Portfolio "

  subscriptions.each do |channel|

    portfolio.name << channel[0...2] + " "

  end

  portfolio.save
  

  subscriptions.each do |channel|

    channel_subscription = ChannelSubscription.new
    channel_subscription.name = channel
    channel_subscription.portfolio_id = portfolio.id
    channel_subscription.save

  end


end

all_channel_subscriptions = Array.new

channels.each do |channel|

  channel_subscription = ChannelSubscription.new

  channel_subscription.name = channel
  channel_subscription.save
    
  all_channel_subscriptions << channel_subscription

 end

############################################################ Customer

loyalty_level = [1,2,3,4,5]
billing_status = ["GOOD", "BAD"]

all_customers = Array.new

2..200.times do |i|

  
  customer = Customer.new

  customer.account_number = "#{i}"
  customer.loyalty_level = loyalty_level.sample
  customer.billing_status = billing_status.sample
  customer.save
  
  x = [1, 2, 3, 4].sample

  x.times do |random|
    customer_subscription = CustomerSubscription.new
    customer_subscription.customer_id = customer.id
    customer_subscription.channel_subscription_id = all_channel_subscriptions[random].id
    customer_subscription.save
  end

  

end

############################################################ Portfolio


