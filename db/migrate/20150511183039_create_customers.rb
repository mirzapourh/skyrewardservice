class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :account_number
      t.integer :loyalty_level
      t.string :billing_status

      t.timestamps null: false
    end
  end
end
