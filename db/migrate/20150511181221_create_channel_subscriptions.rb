class CreateChannelSubscriptions < ActiveRecord::Migration
  def change
    create_table :channel_subscriptions do |t|
      t.belongs_to :portfolio
      t.string :name

      t.timestamps null: false
    end
  end
end
