class CreateRewards < ActiveRecord::Migration
  def change
    create_table :rewards do |t|
      t.belongs_to :channel_subscription
      t.string :name

      t.timestamps null: false
    end
  end
end
