def page_is_new_view?
  page.should have_content "Create a New View"
  page.should have_field "Name"
  page.should have_button "Create View"
end

def page_is_create_view?
  page.should have_content "Successfully created view"
end