# rspec descriptions of pages associated with the welcome controller
def page_is_welcome_index?
  # puts "  #=> page is welcome#index #{}"
  page.should have_text "Welcome to Daniel Baldwin's Final Year Project"
  # page.should have_link "begin"
end