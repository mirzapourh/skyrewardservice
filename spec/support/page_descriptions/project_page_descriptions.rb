def page_is_projects_index?(user)
  page.should have_content "Projects"
  page.should have_link "New Project"

  # check that all of user's projects are displayed
  user.projects.each do |project|
    page.should have_link "show-link-#{project.id}"

    page.should have_content project.name
    page.should have_content project.database

    page.should have_link "edit-link-#{project.id}"
    page.should have_link "delete-link-#{project.id}"
  end
end

def page_is_new_project?
  page.should have_content "New Project"

  page.should have_field "Name"
  page.should have_field "Database"

  page.should have_button "Create Project"
end

def page_is_edit_project?(*project)
  page.should have_content "Edit Project"
  
  page.should have_field "Name"

  page.should have_field "Database"

    unless project.empty?
      page.find_field("Name").value.should == project[0].name
      page.find_field("Database").value.should == project[0].database
    end

  page.should have_button "Update Project"
end

def page_is_show_project?(project)
  page.should have_content "Project: #{project.name}"
  page.should have_content "DB: #{project.database}"


  page.should have_content "Select a tool from above"
  page.should have_content "Models"
  page.should have_content "Create a new model"
  page.should have_content "Edit a model's name, attributes and associations"
  page.should have_content "Controllers"
  page.should have_content "Create a new controller"
  page.should have_content "Edit a controller's name, actions and associations"
  page.should have_content "Views"
  page.should have_content "Create a new view"
  page.should have_content "Edit a view's name and associations"
end