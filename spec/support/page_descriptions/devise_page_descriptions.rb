# rspec descriptions of pages associated with devise
def page_is_new_registration?
  # puts "  #=> page is new registration #{}"
  page.should have_text "Sign up"

  page.should have_field "Email"
  page.should have_field "Password"
  page.should have_field "Password confirmation"

  page.should have_button "Sign up"
end

def page_is_edit_registration?(user)
  # puts "  #=> page is new registration #{}"
  page.should have_text "Edit User"

  page.should have_field "Email"
  find_field("Email").value.should == user.email
  
  page.should have_field "Password"
  page.should have_field "Password confirmation"

  page.should have_button "Update"
end

def page_is_new_session?
  # puts "  #=> page is new session #{}"
  page.should have_text "Sign in"

  page.should have_field "Email"
  page.should have_field "Password"

  page.should have_field "Remember me"

  page.should have_button "Sign in"
end

