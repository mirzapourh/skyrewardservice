def page_is_new_model?
  page.should have_content "Create a New Model"
  page.should have_field "Name"
  page.should have_content "Attributes"
  page.should have_css "a#add-att-link"
end

def page_is_create_model?
  page.should have_content "Successfully created model"
end

def page_is_models_index?(project)
  puts "## models index ##"
  puts "## no models to list ##" if project.models.size == 0
  
  page.should have_content "List Models"
  
  project.models.each do |model|
    puts "## model: #{model.id} ##"
    
    page.should have_content model.name

    page.should have_button "actions-dropdown-#{model.id}"

    # TODO: webkit has a slight bug to do with being unable to find elements that get hidden
    # as such I can't test for the links inside actions-dropdown
    # see: https://github.com/thoughtbot/capybara-webkit/issues/494

    # click_button "actions-dropdown-#{model.id}"

    # page.should have_link "details-link-#{model.id}"
    # page.should have_link "association-link-#{model.id}"
    # page.should have_link "delete-link-#{model.id}"
  end
end